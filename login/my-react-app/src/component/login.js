import React, { Component } from 'react';

class LogIn extends Component{

constructor(props){
 super(props);

 this.state = { 
     fullname: "Enter your name",
     email: "Enter your email ID",
     Password:"secret password",
     phone : "9876543211",
    

 }
}

// handlename = (event) => {
  
//   this.setState({ fullname: event.target.value })
// }

// handleemail = (event) => {
  
//   this.setState({ email: event.target.value })
// }

// handlenumber = (event) => {
  
//   this.setState({ number: event.target.value })
// }

// handlemessage = (event) => {
  
//   this.setState({ message: event.target.value })
// }

handlechangeall = (event) =>{
 this.setState ( { [event.target.name] :event.target.value  } )
}

handlesubmit = (event) => {
 alert (`my name is ${this.state.fullname}. 
  My email id is ${this.state.email}
  My password is ${this.state.Password}
  My mobile number is ${this.state.phone}.
 
  
  `);
 // alert( JSON.stringify(this.state));
 console.log( JSON.stringify(this.state));
 event.preventDefault();
}

render(){
 return(
  <div>
   <form  className="col-lg-5 border p-4 m-5 mx-auto shadow" id="form" onSubmit = {this.handlesubmit} method="post" >
   <h3>LogIn Form</h3>
    <br/>
    <label> Full Name </label> <br/>
    <input  type="text" name="fullname"  value={this.state.fullname}  
      onChange={this.handlechangeall} /> <br/>

    <label> Email </label><br/>
    <input  type="email" name="email" value= {this.state.email} 
      onChange={this.handlechangeall} /> <br/>

    <label> Password</label><br/>
    <input  type="text" name="password" value= {this.state.Password} 
            onChange={this.handlechangeall} /> <br/>


    <label> Mobile </label><br/>
    <input  type="number" name="phone" value= {this.state.phone} 
            onChange={this.handlechangeall} /> <br/>

      <br/> <br/>

    <input type="submit" value="Send" />
   </form>
  </div>
 )

}
}
export default LogIn;